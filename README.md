# README #

1/3スケールのSONY SMC-777風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- SONY

## 発売時期
- 1983年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/SMC-777)
- [コンピュータ博物館](http://museum.ipsj.or.jp/computer/personal/0099.html)
- [懐かしのホビーパソコン紹介](https://twitter.com/i/events/784708944019136512)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_smc-777/raw/d8462928ca974b3ca2099ec593f59fbc1f88ff50/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_smc-777/raw/d8462928ca974b3ca2099ec593f59fbc1f88ff50/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_smc-777/raw/d8462928ca974b3ca2099ec593f59fbc1f88ff50/ExampleImage.png)
